var studentsAndPoints = 
['Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0];

function Student(student, point) {
	this.student = student;
	this.point = point;
	}
Student.prototype.valueOf = function() {
	return this.point;
}
Student.prototype.show = function() {
	console.log('Студент ' + this.student + ' набрал ' + this.point + ' баллов');
}

function StudentList (groupTitle, arrayStudents) {
	this.groupTitle = groupTitle;
	this.arrayStudents = [];
	if (arguments[1]) {
			for ( var i = 0; i < arrayStudents.length; i += 2) {
				this.arrayStudents.push(new Student(arrayStudents[i], arrayStudents[i + 1]));
			}
	} 
}

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;


StudentList.prototype.add = function(student, point) {
	this.arrayStudents.push(new Student(student, point));
}

StudentList.prototype.show = function() {

	console.log("Группа " + this.groupTitle + this.studentsLength());
		this.arrayStudents.forEach(function(student){
		student.show();
	});
}

StudentList.prototype.studentsLength = function() {
		if (this.arrayStudents.length < 5) {
			return " (" + this.arrayStudents.length + " студента):"
		} else {
			return " (" + this.arrayStudents.length + " студентов):"
		}
	}

var hj2 = new StudentList("HJ-2",['Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0]);

hj2.add('Владимир Честный', 20);
hj2.add('Олег Шапошников', 100);

var html7 = new StudentList("HTML-7");

html7.add('Павел Бова', 70);
html7.add('Роман Музалев', 40);
html7.add('Виктория Лев', 50);
html7.add('Анастасия Молчанова', 50);


console.log("----------------------------До--------------------------")
html7.show();
hj2.show();

StudentList.prototype.removeStudent = function(nameStudent,nameAddGroup) {

	 this.arrayStudents.forEach(function(elem,i){
	 	return function(){
	 		if (elem.student === nameStudent) {
				nameAddGroup.add(elem.student, elem.point);
				this.arrayStudents.splice(i, 1);
			}
	 	}

	});
}
hj2.removeStudent("Ирина Овчинникова", html7);

console.log("----------------------------После--------------------------")
html7.show();
hj2.show();
var a = new Student("Ольга", 90);

console.log("Дополнительное задание");

StudentList.prototype.maxPoint = function() {
	return Math.max.apply(this, this.arrayStudents);
}

StudentList.prototype.max = function(){
	var max = this.maxPoint();
	return this.arrayStudents.find(function(student) {
		if (+student == max) {
			return student;
		}
	});
}
console.log(html7.max());
